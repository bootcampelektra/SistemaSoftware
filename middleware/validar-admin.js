
const {
    response, 
    request 
} = require('express');


const jwt = require('jsonwebtoken');


const validaAdmin = (req = request, res = response, next) => {
    
    const token = req.header("x-token");

    // Si no se encuentra el token generamos un error
    if (!token) {
        return res.status(401).json({
            msg: "No hay token en la petición"
        });
    }

    
    try {
        jwt.verify(token, process.env.SECRETKEYJWT);
       const payload = jwt.decode(token, process.env.SECRETKEYJWT);
       if(payload['id'] === 1){
        next();
       }else{
        return res.status(401).json({
            msg: "No puedes realizar esta operación"
        });
       }
        
    } catch (error) {
        
        return res.status(401).json({
            msg: "Token no valido",
            error
        });
    }
}

// Exportamos modulos
module.exports = {
    validaAdmin
}
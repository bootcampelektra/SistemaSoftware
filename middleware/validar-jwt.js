
const {
    response, 
    request 
} = require('express');


const jwt = require('jsonwebtoken');


const validatJWT = (req = request, res = response, next) => {
    
    const token = req.header("x-token");

    // Si no se encuentra el token generamos un error
    if (!token) {
        return res.status(401).json({
            msg: "No hay token en la petición"
        });
    }

    
    try {
        jwt.verify(token, process.env.SECRETKEYJWT);
       // const payload = jwt.decode(token, process.env.SECRETKEYJWT);
       // console.log(payload['id']);
        next();
        
    } catch (error) {
        
        return res.status(401).json({
            msg: "Token no valido",
            error
        });
    }
}

// Exportamos modulos
module.exports = {
    validatJWT
}
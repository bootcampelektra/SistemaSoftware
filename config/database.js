const mongoose = require("mongoose");

const DB_URI = `mongodb://localhost:27017/elektra`

module.exports = () => {

    const connect = () => {

        mongoose.connect(
            DB_URI,
            {
                keepAlive: true,
                useNewUrlParser: true
            },
            (err) => {
                if(err){
                   console.log('Error:', err)
                }else{
                    console.log('Conexión correcta');
                }
            }
        )
    }


    connect();

}
const mongoose = require('mongoose');
var Schema = mongoose.Schema;
const programador = require('../models/programadores');

const bitacoraSchema = new mongoose.Schema({
    inicio: {
        type: String,
        required: true
    },
    final: {
        type: String,
        required: true
    },
    nombre: {
        type: String,
        required: true
    },
    programador: { type: Schema.ObjectId, ref: "programadores", required: true }
})

module.exports = mongoose.model('bitacora', bitacoraSchema);
const mongoose = require('mongoose');
var Schema = mongoose.Schema;
const persona = require('../models/persona');

const programadorSchema = new mongoose.Schema({
    tipo: {
        type: String,
        required: true
    },
    persona: { type: Schema.ObjectId, ref: "persona", unique : true }
})

module.exports = mongoose.model('programadores', programadorSchema);
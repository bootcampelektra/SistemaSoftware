const mongoose = require('mongoose');
var Schema = mongoose.Schema;
const persona = require('../models/persona');

const registroSchema = new mongoose.Schema({
    horaEntreda: {
        type: String,
        required: true
    },
    horaSalida: {
        type: String,
        required: true
    },
    persona: { type: Schema.ObjectId, ref: "persona" }
})

module.exports = mongoose.model('registro', registroSchema);
const model = require('../models/persona');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');


exports.ConsultarDatos = (req, res) =>{
    model.find({}, (err, docs) =>{
        res.json(docs);
    })
}

exports.ObtenerDato = (req,res) => {
    const id = req.params.id;
    model.findById(id , (error, docs) => {
       res.json(docs);
    })
}


exports.AgregarDatos = (req, res) =>{
    const {nombre, apellido, edad, telefono, correo, contraseña, tipo} = req.body;

    if (!nombre) {
        return res.status(401).json({
          msg: "No puede ir el campo nombre vacío"
        });
    
      }
    
      if (!apellido) {
        return res.status(401).json({
          msg: "No puede ir el campo apellido vacío"
        });
    
      }
    
    
      if (!isNaN(edad) === false || !edad) {
    
        return res.status(401).json({
          msg: "No puede ir el dato edad vacío y debe ser númerico"
        });
      }
    
    
      if (!isNaN(telefono) === false || !telefono ) {
    
        return res.status(401).json({
          msg: "No puede ir el dato teléfono vacío y debe ser númerico"
        });
      }
    
      if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(correo)) {
        return res.status(401).json({
          msg: "Proporcione un correo correcto"
        });
      }
    
     
      if (!contraseña) {
        return res.status(401).json({
          msg: "No puede ir el campo contraseña  vacío"
        });
    
      }

      if (!isNaN(tipo) === false || !tipo ) {
    
        return res.status(401).json({
          msg: "No puede ir el dato tipo vacío y debe ser númerico"
        });
      }


       // creamos salt para poder encryptar 
    const salt = bcrypt.genSaltSync(10);
    // En la variable cryptPass guardamos el encryptado de la constraseña con el salto
    cryptPass = bcrypt.hashSync(contraseña, salt);


    const persona = new model({
        nombre: nombre,
        apellido: apellido,
        edad: edad,
        telefono: telefono,
        correo: correo,
        contraseña: cryptPass,
        tipo: tipo
    });
    

    persona
    .save()
    .then((data) =>{
        res.json({
            msg: "Persona agregada",
            data: data
        })
    }).catch((error) =>{
        res.json(error)
    })
  }

 


  exports.IniciarSesion = async (req, res) =>{
    const {correo, passwd} = req.body;

    if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(correo)) {
        return res.status(401).json({
          msg: "Proporcione un correo correcto"
        });
      }
    
      if (!passwd) {
        return res.status(401).json({
          msg: "No puede ir el campo passwd vacío"
        });
    
      }


   
    async function buscar(){
    return new Promise((resolve, reject) => {   
    model.find({
        $and:
         [
           {correo: correo}
         ]} , (error, docs) =>{
          if(!error){
            if(docs[0] == null){
                res.json({msg :"Correo o contraseña incorrectos"})
            }else{
                resolve({ tipo : docs[0]['tipo'],
              passwd: docs[0]['contraseña']});
            } 
          }else{
            res.json({error})
          } 
    })
    })
    }

    const datosP = await buscar();
    const id = datosP['tipo'];
    if(bcrypt.compareSync(passwd, datosP['passwd'])){

     if(id){

        let token = jwt.sign({
            id
        }, process.env.SECRETKEYJWT , {});

        res.json({
            token
        });
    }else{
        res.json({
            msg: "Error"
        })
    }

  }else{
    res.json({
      msg: "Contraseña incorrecta"
  })
  }

  } 


  exports.ModificarDatos = (req, res) =>{
    const {id} = req.params;
    const {nombre, apellido, edad, telefono, correo, contraseña} = req.body;


    if (!nombre) {
        return res.status(401).json({
          msg: "No puede ir el campo nombre vacío"
        });
    
      }
    
      if (!apellido) {
        return res.status(401).json({
          msg: "No puede ir el campo apellido vacío"
        });
    
      }
    
    
      if (!isNaN(edad) === false || !edad) {
    
        return res.status(401).json({
          msg: "No puede ir el dato edad vacío y debe ser númerico"
        });
      }
    
    
      if (!isNaN(telefono) === false || !telefono ) {
    
        return res.status(401).json({
          msg: "No puede ir el dato teléfono vacío y debe ser númerico"
        });
      }
    
      if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(correo)) {
        return res.status(401).json({
          msg: "Proporcione un correo correcto"
        });
      }
    
     
      if (!contraseña) {
        return res.status(401).json({
          msg: "No puede ir el campo contraseña  vacío"
        });
    
      }



    // creamos salt para poder encryptar 
    const salt = bcrypt.genSaltSync(10);
    // En la variable cryptPass guardamos el encryptado de la constraseña con el salto
    cryptPass = bcrypt.hashSync(contraseña, salt);

    model.findByIdAndUpdate(id, {
        nombre: nombre,
        apellido: apellido,
        edad: edad,
        telefono: telefono,
        correo: correo,
        contraseña: cryptPass
    }, function (err, docs){
        if (err){
            res.json(err);
        }else{
            res.json({
                msg: "Persona Modificada",
                data: docs
            })
        }
    })   
}


exports.EliminarDatos = (req, res) =>{
    const {id} = req.params;
    model.findByIdAndDelete(id, function (err, docs) {
        if(err){
            res.json(err);
        }else{
            res.json({
                msg: "Persona Elimiada",
                data: docs
            })
        }
    })
}

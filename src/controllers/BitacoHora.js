const model = require('../models/BitacoraHora');
const persona = require('../models/persona');

var hora = new Date();
 

var now = hora.toLocaleString();


exports.ConsultarDatos = (req, res) =>{
    model.find({}, (err, docs) =>{
    
                res.json(docs);

    })
}

exports.ObtenerDato = (req,res) => {
    const id = req.params.id;
     model.findById(id, (err, docs) =>{
        persona.populate(docs, { path: "persona" }, function (err, registro){
                res.json(registro);
        })
    })
}


exports.AgregarEntreda = (req, res) =>{
    const {id} = req.body;

    if (!id) {
        return res.status(401).json({
          msg: "No puede ir el campo id vacío"
        });
    
      }


    const registro = new model({
        persona: id,
        horaEntreda: now,
        horaSalida: "-----"
    });
    registro
    .save()
    .then((data) =>{
        res.json({
            msg: "Hora de entrada agregada",
            data: data
        })
    }).catch((error) =>{
        res.json(error)
    })
  }

 


  exports.HoraSalida = (req, res) =>{
    const {id} = req.params;

    if (!id) {
        return res.status(401).json({
          msg: "No puede ir el campo id vacío"
        });
    
      }

    model.findByIdAndUpdate(id, {
        horaSalida : now
    }, function (err, docs){
        if (err){
            res.json(err);
        }else{
            res.json({
                msg: "Hora Salida modificada",
                data: docs
            })
        }
    })   
}


exports.EliminarDatos = (req, res) =>{
    const {id} = req.params;
    model.findByIdAndDelete(id, function (err, docs) {
        if(err){
            res.json(err);
        }else{
            res.json({
                msg: "Registro Eliminada",
                data: docs
            })
        }
    })
}

const model = require('../models/programadores');
const persona = require('../models/persona');



exports.ConsultarDatos = (req, res) =>{
    model.find({}, (err, docs) =>{
        persona.populate(docs, { path: "persona" }, function (err, registro){
                res.json(registro);
        })
    })
}

exports.ObtenerDato = (req,res) => {
    const id = req.params.id;
     model.findById(id, (err, docs) =>{
        persona.populate(docs, { path: "persona" }, function (err, registro){
                res.json(registro);
        })
    })
}


exports.AgregarDatos = async (req, res) =>{
    const {tipo, id} = req.body;


    if (!tipo) {
        return res.status(401).json({
          msg: "No puede ir el campo tipo vacío"
        });
    
      }


      if (!id) {
        return res.status(401).json({
          msg: "No puede ir el campo id vacío"
        });
    
      }

    const programador = new model({
        persona: id,
        tipo: tipo
    });
    programador
    .save()
    .then((data) =>{
        res.json({
            msg: "Programador agregado",
            data: data
        })
    }).catch((error) =>{
        res.json(error)
    })


  }

 


  exports.ModificarDatos = (req, res) =>{
    const {id} = req.params;
    const {tipo} = req.body;


    if (!tipo) {
        return res.status(401).json({
          msg: "No puede ir el campo tipo vacío"
        });
    
      }


    model.findByIdAndUpdate(id, {
        tipo : tipo
    }, function (err, docs){
        if (err){
            res.json(err);
        }else{
            res.json({
                msg: "Programador modificado",
                data: docs
            })
        }
    })   
}


exports.EliminarDatos = (req, res) =>{
    const {id} = req.params;
    model.findByIdAndDelete(id, function (err, docs) {
        if(err){
            res.json(err);
        }else{
            res.json({
                msg: "Programador Eliminado",
                data: docs
            })
        }
    })
}

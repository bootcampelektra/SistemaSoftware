const model = require('../models/bitacora');
const programadores = require('../models/programadores');

var hora = new Date();
var now = hora.toLocaleString();


exports.ConsultarDatos = (req, res) =>{
    model.find({}, (err, docs) =>{
        programadores.populate(docs, { path: "programadores" }, function (err, bitacora){
                res.json(bitacora);
        })
    })
}

exports.ObtenerDato = (req,res) => {
    const id = req.params.id;
     model.findById(id, (err, docs) =>{
        programadores.populate(docs, { path: "programadores" }, function (err, registro){
                res.json(registro);
        })
    })
}


exports.AgregarEntrada = (req, res) =>{
    const {id, nombre} = req.body;


    if (!id) {
        return res.status(401).json({
          msg: "No puede ir el campo id vacío"
        });
    
      }

      if (!nombre) {
        return res.status(401).json({
          msg: "No puede ir el campo nombre vacío"
        });
    
      }

    const registro = new model({
        programador: id,
        nombre: nombre,
        inicio: now,
        final: "-------"
    });
    registro
    .save()
    .then((data) =>{
        res.json({
            msg: "Inicio de bitacora agregada",
            data: data
        })
    }).catch((error) =>{
        res.json(error)
    })
  }

 


  exports.AgregarSalida = (req, res) =>{
    const {id} = req.params;
    model.findByIdAndUpdate(id, {
        final : now
    }, function (err, docs){
        if (err){
            res.json(err);
        }else{
            res.json({
                msg: "Proyecto finalizado",
                data: docs
            })
        }
    })   
}


exports.EliminarDatos = (req, res) =>{
    const {id} = req.params;
    model.findByIdAndDelete(id, function (err, docs) {
        if(err){
            res.json(err);
        }else{
            res.json({
                msg: "Bitacora de proyecto Eliminada",
                data: docs
            })
        }
    })
}

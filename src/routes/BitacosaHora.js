const express = require('express')
const controller = require("../controllers/BitacoHora")
const cors = require('cors');
const {
    validatJWT
} = require('../../middleware/validar-jwt');
const {
    validaAdmin
} = require('../../middleware/validar-admin');
const {
    validaAdminClient
} = require('../../middleware/valida-admindClient');

const router = express.Router();
router.use(cors());

router.get('/registro/solo/:id', [validatJWT], controller.ObtenerDato);

router.get('/registro/todos', [validatJWT], controller.ConsultarDatos);

router.post('/registro/entrada', [validaAdminClient] ,controller.AgregarEntreda);

router.put('/registro/horaSalida/:id', [validaAdminClient] ,controller.HoraSalida);

router.delete('/registro/eliminar/:id', [validaAdmin] ,controller.EliminarDatos);


module.exports = router


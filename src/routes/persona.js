const express = require('express');
const controller = require("../controllers/persona");

const {
    validatJWT
} = require('../../middleware/validar-jwt');
const {
    validaAdmin
} = require('../../middleware/validar-admin');
const {
    validaAdminClient
} = require('../../middleware/valida-admindClient');
const cors = require('cors');


const router = express.Router();


router.use(cors());



router.get('/persona/solo/:id',  [validatJWT], controller.ObtenerDato);

router.get('/persona/todos', [validatJWT] ,controller.ConsultarDatos);

router.post('/persona/agregar', [validaAdmin], controller.AgregarDatos);

router.post('/persona/validar', controller.IniciarSesion);

router.put('/persona/modificar/:id', [validaAdminClient]  , controller.ModificarDatos);

router.delete('/persona/eliminar/:id',  [validaAdmin] ,controller.EliminarDatos);


module.exports = router;
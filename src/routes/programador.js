const express = require('express')
const controller = require("../controllers/programadores")
const cors = require('cors');
const {
    validatJWT
} = require('../../middleware/validar-jwt');
const {
    validaAdmin
} = require('../../middleware/validar-admin');
const {
    validaAdminClient
} = require('../../middleware/valida-admindClient');

const router = express.Router();
router.use(cors());

router.get('/programador/solo/:id', [validatJWT],  controller.ObtenerDato);

router.get('/programador/todos', [validatJWT], controller.ConsultarDatos);

router.post('/programador/guardar', [validaAdminClient], controller.AgregarDatos);

router.put('/programador/modificar/:id', [validaAdminClient] ,controller.ModificarDatos);

router.delete('/programador/eliminar/:id', [validaAdmin] ,controller.EliminarDatos);


module.exports = router


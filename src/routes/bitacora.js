const express = require('express');
const controller = require("../controllers/bitacora");
const cors = require('cors');
const {
    validatJWT
} = require('../../middleware/validar-jwt');
const {
    validaAdmin
} = require('../../middleware/validar-admin');
const {
    validaAdminClient
} = require('../../middleware/valida-admindClient');

const router = express.Router();
router.use(cors());

router.get('/bitacora/solo/:id', [validatJWT] , controller.ObtenerDato);

router.get('/bitacora/todos', [validatJWT] ,controller.ConsultarDatos);

router.post('/bitacora/entrada', [validaAdminClient] ,controller.AgregarEntrada);

router.put('/bitacora/final/:id', [validaAdminClient] ,controller.AgregarSalida);

router.delete('/bitacora/eliminar/:id', [validaAdmin] ,controller.EliminarDatos);


module.exports = router


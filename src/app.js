const express = require('express');
const mongoose = require('mongoose');
const rutasPersona = require('../src/routes/persona');
const rutasRegistro = require('../src/routes/BitacosaHora');
const rutasProgramador = require('../src/routes/programador');
const rutasBitacora = require('../src/routes/bitacora');
const cors = require('cors');
require("dotenv").config();


const app = express();
const port = 3000;

app.use(express.json());
app.use(cors());


//Rutas 
app.use('/api',rutasPersona);
app.use('/api', rutasRegistro);
app.use('/api', rutasProgramador);
app.use('/api', rutasBitacora);

app.get('/', (req,res) => res.send('Hello World') );


mongoose.connect(process.env.MONGODB_URI)
.then(() =>{
    console.log("Conexión éxitosa");
}).catch((error) =>{
    console.groupCollapsed(error);
})


app.listen(port, () =>  console.log(`Servidor arriba en el puerto: ${port}`))